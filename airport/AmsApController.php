<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsApController.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 21.03.2016
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsApController.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */

date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}

define('APP_HOME', dirname(dirname((__FILE__))));
define('SLASH', $slash);

ini_set("include_path", ini_get("include_path") . $delim . '/home/iordanov/php');

ini_set('include_path', ini_get('include_path') . $delim . '../' .
        $delim . '/home/iordanov/common/lib' . $delim . '/home/iordanov/common/lib/iiordan' .
        $delim . '/home/iordanov/common/lib/ams' .
        $delim . '/home/iordanov/common/lib/ams/al' .
        $delim . '/home/iordanov/common/lib/ams/wad' .
        $delim . '/home/iordanov/common/lib/ams/models' .
        $delim . '/home/iordanov/common/lib/log4php' .
        $delim . '/home/iordanov/common//lib/log4php/configurators');



$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;

ini_set("display_errors", "1");

ob_start();

$mn = "AmsApController.php";
//--- Include CORS
require_once("rest_cors_header.php");

//require_once("AmsWadConnection.php");
require_once("AmsLogger.php");
require_once("Functions.php");
//---Handlers
require_once("AmsApRestHandler.class.php");
AmsLogger::logBegin($mn);

$view = "";
$id = null;

if (isset($_REQUEST["view"]))
    $view = $_REQUEST["view"];
if (isset($_REQUEST["id"]))
    $id = $_REQUEST["id"];

AmsLogger::log($mn, "view=" . $view . ", id=" . $id);

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    $restHendler = new AmsApRestHandler();
    $restHendler->Option();
    AmsLogger::logEnd($mn);
} else {

    // get the HTTP method, path and body of the request
    $method = $_SERVER['REQUEST_METHOD'];
    AmsLogger::log($mn, "method=" . $method);
    // read JSon input
    $payloadJson;
    try{
        $payload = file_get_contents('php://input');
        if(isset($payload)){
            $payloadJson = json_decode($payload);
            AmsLogger::log($mn, "payload=" . $payload);
        }
    } catch (Exception $ex) {
        AmsLogger::logError($mn,  $ex);
    }
    
    /*
      controls the RESTful services URL mapping
     */
    switch ($view) {
        case "ping":
            // to handle REST Url /pcpd/
            $restHendler = new AmsApRestHandler();
            $restHendler->Ping($id);
            AmsLogger::log($mn, "ping executed");
            break;
        case "countrygvdt":
        case "countrylist":
            // to handle REST Url /pcpd/
            $restHendler = new AmsApRestHandler();
            $response = array();
            try {
                if($view == "countrylist")
                {
                     $restHendler->CountyList();
                }
                else
                    $restHendler->GvdtCountyList();
            } catch (Exception $ex) {
                $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
        
            AmsLogger::log($mn, "response= " . $response);
            //AmsLogger::logEnd($mn);
            break;
        case "country_region_list":
            // to handle REST Url /pcpd/
            $restHendler = new AmsApRestHandler();
            $response = array();
            try {
                    $restHendler->CountryStateList($payloadJson->country_id);
            } catch (Exception $ex) {
                $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
        
            AmsLogger::log($mn, "response= " . $response);
            //AmsLogger::logEnd($mn);
            break;
        case "ap_list_by_state":
            // to handle REST Url /pcpd/
            $restHendler = new AmsApRestHandler();
            $response = array();
            try {
                    $restHendler->ApListByState($payloadJson->state_id);
            } catch (Exception $ex) {
                $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
        
            AmsLogger::log($mn, "response= " . $response);
            //AmsLogger::logEnd($mn);
            break;
       case "ap_list_by_country":
            // to handle REST Url /pcpd/
            $restHendler = new AmsApRestHandler();
            $response = array();
            try {
                    $restHendler->ApListByCountry($payloadJson->country_id);
            } catch (Exception $ex) {
                $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
        
            AmsLogger::log($mn, "response= " . $response);
            //AmsLogger::logEnd($mn);
            break;
       case "airport":
            // to handle REST Url /pcpd/
            $restHendler = new AmsApRestHandler();
            $response = array();
            try {
                    $restHendler->ApListById($payloadJson->airport_id);
            } catch (Exception $ex) {
                $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
        
            AmsLogger::log($mn, "response= " . $response);
            //AmsLogger::logEnd($mn);
            break;
        case "ap_rw_list":
            // to handle REST Url /pcpd/
            $restHendler = new AmsApRestHandler();
            $response = array();
            try {
                    $restHendler->RwListByAirport($payloadJson->airport_id);
            } catch (Exception $ex) {
                $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
        
            AmsLogger::log($mn, "response= " . $response);
            //AmsLogger::logEnd($mn);
            break;
        
        case "airports_db_all":
             $restHendler = new AmsApRestHandler();
            $response = array();
            try {
                $restHendler->AirportListDbAll();
            } catch (Exception $ex) {
                $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
        
            AmsLogger::log($mn, "response= " . $response);
            //AmsLogger::logEnd($mn);
            break;
        case "login":
            // to handle REST Url /pcpd/
            $restHendler = new AmsApRestHandler();
            
            if (isset($payloadJson))
                $restHendler->Login($payloadJson->username, $payloadJson->password);

            break;
            
        case "refreshtoken":
            // to handle REST Url /pcpd/
            $restHendler = new AmsApRestHandler();
            
            if (isset($payloadJson))
                $restHendler->refreshToken();

            break;
        default:
            $response = new Response("error", "There is something wrong in your request."." No heandler for view: " . $view);
            AmsLogger::log($mn, "No heandler for view: " . $view);
            break;
    }
}
AmsLogger::logEnd($mn);
