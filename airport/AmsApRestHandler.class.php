<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsApRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 21.03.2016
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsApRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once 'Response.class.php';
require_once("AmsConnection.php");
require_once("AmsLogger.php");
require_once("JwtAuth.php");
require_once("AmsUser.class.php");
require_once("UserModel.class.php");
require_once 'GVDataTable.class.php';
require_once("WadAirport.class.php");
require_once("WadAirportRunway.class.php");


/**
 * Description of AmsApRestHandler
 *
 * @author IZIordanov
 */
class AmsApRestHandler extends SimpleRest{
    
    // <editor-fold defaultstate="collapsed" desc="No JWT Tocken Methods">
    
    public function Option() {
        $mn = "AmsApRestHandler::Option()";
        $response = new Response("success", "Service working.");
        
        $rh = new AmsApRestHandler();
        $rh->EncodeResponce($response);
    }
    
    public function Ping() {
        $mn = "AmsApRestHandler::Ping()";
        AmsLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsConnection::dbConnect();
            if (isset($conn)) {
                AmsLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            }
            else
            {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }
            
        } catch (Exception $ex) {
            AmsLogger::logError($mn,  $ex);
            $response = new Response($ex);
        }
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function GvdtCountyList() {
        $mn = "AmsApRestHandler::GvdtCountyList()";
        AmsLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($mn);
            
            $dtData = new GvdtApByCountry();
            $dtData->GetData($conn, $logModel);
            $response->addData("gvdt",$dtData);
            //$response = $dtData->toJSON();
            
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CountyList() {
        $mn = "AmsApRestHandler::CountyList()";
        AmsLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($mn);
            $sql = "SELECT cr.country_name as cname, cr.country_iso2 as iso2, 
                    cr.region, cr.sub_region as subRegion, 
                    cr.country_icao as icao, cr.wikilink, 
                    count(a.airport_id) as airports, 
                    sum(if(a.active>0,1,0) ) as apActive,
                    cr.country_id as id
                FROM iordanov_ams_wad.air_airport a
                join iordanov_ams_wad.cfg_country cr on cr.country_id = a.country_id
                 where 1=?
                group by id, cname, iso2
                order by cname, iso2, apActive desc, airports desc";
            $bound_params_r = ["i", 1];
            $ret_regions = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("countyList",$ret_regions);
            
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CountryStateList($country_id) {
        $mn = "AmsApRestHandler::CountryStateList(".$country_id.")";
        AmsLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($mn);
            $sql = "SELECT cs.state_name as sname, cs.state_iso as iso, 
                     cs.state_wikiurl as wikiLinkUrl, cs.country_id as countryId,
                    count(a.airport_id) as airports, 
                    sum(if(a.active>0,1,0) ) as apActive,
                    cs.state_id as stateId
                FROM iordanov_ams_wad.cfg_country_state cs 
                left join iordanov_ams_wad.air_airport a on cs.state_id = a.state_id
		where cs.country_id = ?
                group by sname, iso, stateId
                order by sname, iso, apActive desc, airports desc";
            $bound_params_r = ["i", $country_id];
            $ret_regions = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("countyStateList",$ret_regions);
            
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function ApListByState($stateId) {
        $mn = "AmsApRestHandler::ApListByState(".$stateId.")";
        AmsLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($mn);
            $sql = "SELECT a.airport_id as apId, a.icao, a.iata, a.aname as apName, a.lat, a.lon, t.ap_type_name as apType, st.ams_status_name as apStatus,
            a.home_link as homeUrl, a.wikipedia_link wikiUrl, a.notes, a.utc, a.atype_base apTypeBase,
            a.active, a.ap_type_id as apTypeId, st.ams_status_id as apStatusId,
            c.region, c.sub_region as sunRegion, c.country_icao as countryIcao, c.wikilink as countryWikiLink,
            c.country_name as countryName, c.country_iso2 as countryIso, cst.state_name as stateName, 
            ci.city_name as cityName, ci.population cityPopulation, ci.wiki_url as cityWikiUrl,
            a.country_id as countryId, a.state_id as stateId, a.city_id as cityId,
            r.svg_path_circle as apSvgCircle, r.rw_lines as apSvgRwLines,
            ad.max_rlenght_m as rwLenghtMaxM, ad.max_mtow_kg as rwMtowMaxKg
            FROM iordanov_ams_wad.air_airport a 
            left join iordanov_ams_wad.cfg_airport_type t on t.ap_type_id = a.ap_type_id
            left join iordanov_ams_wad.cfg_airport_ams_status st on st.ams_status_id = a.ams_status
            left join iordanov_ams_wad.cfg_country c on c.country_id = a.country_id
            left join iordanov_ams_wad.cfg_country_state cst on cst.state_id = a.state_id
            left join iordanov_ams_wad.cfg_city ci on ci.city_id = a.city_id
            left join iordanov_ams_wad.v_air_airport_runway_svg_path r on r.airport_id = a.airport_id
            left join iordanov_ams_wad.air_airport_details ad on ad.airport_id = a.airport_id
            where a.state_id = ?
            order by apTypeId desc, icao, iata, apName";
            $bound_params_r = ["i", $stateId];
            $ret_regions = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("airport_list",$ret_regions);
            
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function ApListByCountry($countryId) {
        $mn = "AmsApRestHandler::ApListByCountry(".$countryId.")";
        AmsLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($mn);
            $sql = "SELECT a.airport_id as apId, a.icao, a.iata, a.aname as apName, a.lat, a.lon, t.ap_type_name as apType, st.ams_status_name as apStatus,
            a.home_link as homeUrl, a.wikipedia_link wikiUrl, a.notes, a.utc, a.atype_base apTypeBase,
            a.active, a.ap_type_id as apTypeId, st.ams_status_id as apStatusId,
            c.region, c.sub_region as sunRegion, c.country_icao as countryIcao, c.wikilink as countryWikiLink,
            c.country_name as countryName, c.country_iso2 as countryIso, cst.state_name as stateName, 
            ci.city_name as cityName, ci.population cityPopulation, ci.wiki_url as cityWikiUrl,
            a.country_id as countryId, a.state_id as stateId, a.city_id as cityId,
            r.svg_path_circle as apSvgCircle, r.rw_lines as apSvgRwLines,
            ad.max_rlenght_m as rwLenghtMaxM, ad.max_mtow_kg as rwMtowMaxKg
            FROM iordanov_ams_wad.air_airport a 
            left join iordanov_ams_wad.cfg_airport_type t on t.ap_type_id = a.ap_type_id
            left join iordanov_ams_wad.cfg_airport_ams_status st on st.ams_status_id = a.ams_status
            left join iordanov_ams_wad.cfg_country c on c.country_id = a.country_id
            left join iordanov_ams_wad.cfg_country_state cst on cst.state_id = a.state_id
            left join iordanov_ams_wad.cfg_city ci on ci.city_id = a.city_id
            left join iordanov_ams_wad.v_air_airport_runway_svg_path r on r.airport_id = a.airport_id
            left join iordanov_ams_wad.air_airport_details ad on ad.airport_id = a.airport_id
            where a.country_id = ?
            order by apTypeId desc, icao, iata, apName";
            $bound_params_r = ["i", $countryId];
            $ret_regions = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("airport_list",$ret_regions);
            
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function ApListById($apId) {
        $mn = "AmsApRestHandler::ApListById(".$apId.")";
        AmsLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($mn);
            $sql = "SELECT a.airport_id as apId, a.icao, a.iata, a.aname as apName, a.lat, a.lon, t.ap_type_name as apType, st.ams_status_name as apStatus,
            a.home_link as homeUrl, a.wikipedia_link wikiUrl, a.notes, a.utc, a.atype_base apTypeBase,
            a.active, a.ap_type_id as apTypeId, st.ams_status_id as apStatusId,
            c.region, c.sub_region as sunRegion, c.country_icao as countryIcao, c.wikilink as countryWikiLink,
            c.country_name as countryName, c.country_iso2 as countryIso, cst.state_name as stateName, 
            ci.city_name as cityName, ci.population cityPopulation, ci.wiki_url as cityWikiUrl,
            a.country_id as countryId, a.state_id as stateId, a.city_id as cityId,
            r.svg_path_circle as apSvgCircle, r.rw_lines as apSvgRwLines,
            ad.max_rlenght_m as rwLenghtMaxM, ad.max_mtow_kg as rwMtowMaxKg
            FROM iordanov_ams_wad.air_airport a 
            left join iordanov_ams_wad.cfg_airport_type t on t.ap_type_id = a.ap_type_id
            left join iordanov_ams_wad.cfg_airport_ams_status st on st.ams_status_id = a.ams_status
            left join iordanov_ams_wad.cfg_country c on c.country_id = a.country_id
            left join iordanov_ams_wad.cfg_country_state cst on cst.state_id = a.state_id
            left join iordanov_ams_wad.cfg_city ci on ci.city_id = a.city_id
            left join iordanov_ams_wad.v_air_airport_runway_svg_path r on r.airport_id = a.airport_id
            left join iordanov_ams_wad.air_airport_details ad on ad.airport_id = a.airport_id
            where a.airport_id = ?
            order by apTypeId desc, icao, iata, apName";
            $bound_params_r = ["i", $apId];
            $ret_regions = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("airport_list",$ret_regions);
            
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function AirportListDbAll() {
        $mn = "AmsApRestHandler::AirportListDbAll()";
        AmsLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($mn);
            $sql = "SELECT SQL_CACHE a.airport_id as apId, a.icao, a.iata, a.aname as apName, a.lat, a.lon, t.ap_type_name as apType, st.ams_status_name as apStatus,
            a.home_link as homeUrl, a.wikipedia_link wikiUrl, a.notes, a.utc, a.atype_base apTypeBase,
            a.active, a.ap_type_id as apTypeId, st.ams_status_id as apStatusId,
            c.region, c.sub_region as sunRegion, c.country_icao as countryIcao, c.wikilink as countryWikiLink,
            c.country_name as countryName, c.country_iso2 as countryIso, cst.state_name as stateName, 
            ci.city_name as cityName, ci.population cityPopulation, ci.wiki_url as cityWikiUrl,
            a.country_id as countryId, a.state_id as stateId, a.city_id as cityId,
            r.svg_path_circle as apSvgCircle, r.rw_lines as apSvgRwLines,
            ad.max_rlenght_m as rwLenghtMaxM, ad.max_mtow_kg as rwMtowMaxKg
            FROM iordanov_ams_wad.air_airport a 
            left join iordanov_ams_wad.cfg_airport_type t on t.ap_type_id = a.ap_type_id
            left join iordanov_ams_wad.cfg_airport_ams_status st on st.ams_status_id = a.ams_status
            left join iordanov_ams_wad.cfg_country c on c.country_id = a.country_id
            left join iordanov_ams_wad.cfg_country_state cst on cst.state_id = a.state_id
            left join iordanov_ams_wad.cfg_city ci on ci.city_id = a.city_id
            left join iordanov_ams_wad.v_air_airport_runway_svg_path r on r.airport_id = a.airport_id
            left join iordanov_ams_wad.air_airport_details ad on ad.airport_id = a.airport_id 
                    where 1=?";
            $bound_params_r = ["i", 1];
            $ret_regions = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("airport_list",$ret_regions);
            
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function RwListByAirport($apId) {
        $mn = "AmsApRestHandler::RwListByAirport(".$apId.")";
        AmsLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($mn);
            $sql = "SELECT r.runway_id, r.airport_id, r.rtype, r.rdirection, 
                        r.rsurface, r.elevation_m, r.rwidth_m, r.rlenght_m, r.mtow_kg, r.fixed, 
                        r.timeban, r.timeban_start_h, r.timeban_duractiont_h, r.noise_restriction_level, r.usage_pct,
                        rn.rw_nr, rn.rw_rotation_degree, rn.radius, rn.rw_line, rn.svg_path_circle
                    FROM iordanov_ams_wad.air_airport_runway r
                    left join iordanov_ams_wad.air_airport_runway_nr rn on rn.runway_id = r.runway_id
                    where r.airport_id = ?
                    order by r.mtow_kg desc, r.rlenght_m desc";
            $bound_params_r = ["i", $apId];
            $ret_regions = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("rw_list",$ret_regions);
            
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="JWT Tocken Methods">
    
    public function ApSave($airport) {
        $mn = "AmsApRestHandler::ApSave(".$airport->apId.")";
        AmsLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($mn);
            $ap = new WadAirport();
            $ap->loadById($airport->apId);
            
            $ap->iata = $airport->iata;
            $ap->icao = $airport->icao;
            $ap->apName = $airport->apName;
            $ap->lat = $airport->lat;
            $ap->lon = $airport->lon;
            $ap->homeUrl = $airport->homeUrl;
            $ap->wikiUrl = $airport->wikiUrl;
            $ap->notes = $airport->notes;
            $ap->utc = $airport->utc;
            
            $ap->apTypeId = $airport->apTypeId;
            $ap->countryId = $airport->countryId;
            $ap->stateId = $airport->stateId;
            $ap->cityId = $airport->cityId;
            
            $ap->active = $airport->active;
            $ap->amsStatusId = $airport->amsStatusId;
            
            $ap->save();
            $sql = WadAirport::SQL_SELECT_IAirportModel;
            $bound_params_r = ["i", $airport->apId];
            $ret_regions = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("iAirportModel",$ret_regions[0]);
            
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
}


// <editor-fold defaultstate="collapsed" desc="GV Data Table Classes">

class GvdtApByCountry extends GVDataTableBase {

    public function __construct() {
        parent::__construct();
        $mn = "GvdtApByCountry.construct()";
        AmsLogger::logBegin($mn);
        
        $this->cols = array();
        
        $this->cols[] = new GVColumn('country_name', "Country", GVColumnType::STRING);
        $this->cols[] = new GVColumn('country_iso2', "ISO", GVColumnType::STRING);
        $this->cols[] = new GVColumn('airports', "Airports", GVColumnType::NUMBER);
        $this->cols[] = new GVColumn('ap_active', "Active Airports", GVColumnType::NUMBER);
        $this->cols[] = new GVColumn('country_id', "Cuntry PK", GVColumnType::NUMBER);
       
        //parent::setCols($this->cols);
        $sql = " SELECT cr.country_name, cr.country_iso2, 
                    count(a.airport_id) as airports, sum(if(a.active>0,1,0) ) as ap_active,
                    cr.country_id
                FROM iordanov_ams_wad.air_airport a
                join iordanov_ams_wad.cfg_country cr on cr.country_id = a.country_id
                where 1=? 
                group by cr.country_id, cr.country_name, cr.country_iso2
                order by cr.country_name, cr.country_iso2, ap_active desc, airports desc ";
       
        $this->setSQL($sql);
        //parent::setSQL($sql);
        $this->getSql();
        AmsLogger::log($mn, " SQL = " . $this->getSql());
        $this->setBoundParamsArr(array('i', 1));
        $this->rows = array();
        AmsLogger::logEnd($mn);
    }

}

// </editor-fold>