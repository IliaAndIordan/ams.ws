<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsAlRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 21.03.2016
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsAlRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once 'Response.class.php';
require_once("AmsConnection.php");
require_once("AmsLogger.php");
require_once("JwtAuth.php");
require_once("AmsUser.class.php");
require_once("AmsAirline.class.php");
require_once("UserModel.class.php");
require_once 'GVDataTable.class.php';

/**
 * Description of AmsAlRestHandler
 *
 * @author IZIordanov
 */
class AmsAlRestHandler extends SimpleRest{
    
    // <editor-fold defaultstate="collapsed" desc="No JWT Tocken Methods">
    
    public function Option() {
        $mn = "AmsAlRestHandler::Option()";
        $response = new Response("success", "Service working.");
        
        $rh = new AmsAlRestHandler();
        $rh->EncodeResponce($response);
    }
    
    public static function AuthenticationRequired() {
        $mn = "AmsAlRestHandler::AuthenticationRequired()";
        $response = new Response("error", "Authentication Required.");
        $response->statusCode=401;
        $rh = new AmsAlRestHandler();
        $rh->EncodeResponce($response);
    }
    
    public function Ping() {
        $mn = "AmsAlRestHandler::Ping()";
        AmsLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsConnection::dbConnect();
            if (isset($conn)) {
                AmsLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            }
            else
            {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }
            
        } catch (Exception $ex) {
            AmsLogger::logError($mn,  $ex);
            $response = new Response($ex);
        }
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function AlListForCheck() {
        $mn = "AmsAlRestHandler::AlListForCheck()";
        AmsLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($mn);
            $sql = "SELECT SQL_CACHE al_name, al_code 
                    FROM iordanov_ams_al.ams_airline
                    where 1=?";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("al_list_for_check",$ret_json_data);
            
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function AlCreate($userId, $al_name, $al_code, $hq_airport_Id) {
        $mn = "AmsAlRestHandler::AlCreate()";
        AmsLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($mn);
            $user = new User();
            $user->loadById($userId);
            if(isset($user)){
                $airline = AmsAirline::loadByUserId($userId);
                if(isset($airline)){
                    $response = new Response("error", "Ams Airline exist: " .$airline->getName());
                }
                else{
                    $airline = new AmsAirline();
                    $airline->setName($al_name);
                    $airline->setCode($al_code);
                    $airline->setHqAirportId($hq_airport_Id);
                    $airline->setUserID($userId);
                    $airline->setCreated(new DateTime());
                    $airline->save();
                    $airline = AmsAirline::loadByUserId($userId);
                    
                    $response->addData("airline",$airline);
                }
            }
            
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
     public function GetAirline($airline_id) {
        $mn = "AmsAlRestHandler::GetAirline(".$airline_id.")";
        AmsLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($mn);
            $airline = new AmsAirline();
            $airline->loadById($airline_id);
            $response->addData("airline",$airline);
            
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function SaveAirline($payloadJson) {
        $mn = "AmsAlRestHandler::SaveAirline(".$payloadJson->airline_id.")";
        AmsLogger::logBegin($mn);
        $response = new Response();
        if(isset($payloadJson)){
            try {
                $conn = AmsConnection::dbConnect();
                $logModel = AmsLogger::currLogger()->getModule($mn);
                $airline = new AmsAirline();
                $airline->loadById($payloadJson->airline_id);
                if(isset($payloadJson->uaer_id)){
                   $airline->setUserID($payloadJson->uaer_id);
                }
                if(isset($payloadJson->name)){
                   $airline->setName($payloadJson->name);
                }
                if(isset($payloadJson->code)){
                   $airline->setCode($payloadJson->code);
                }
                if(isset($payloadJson->slogan)){
                   $airline->setSlogan($payloadJson->slogan);
                }
                else{
                    $airline->setSlogan(null);
                }
                if(isset($payloadJson->description)){
                   $airline->setDescription($payloadJson->description);
                }
                else{
                    $airline->setDescription(null);
                }
                 if(isset($payloadJson->hq_airport_id)){
                   $airline->setHqAirportId($payloadJson->hq_airport_id);
                }
                
                $airline->save();
                
                $airline->loadById($payloadJson->airline_id);
                $response->addData("airline",$airline);

            } catch (Exception $ex) {
                logDebug($mn, " Exception = " . $ex);
                $response = new Response($ex);
            }
        }
        else{
            $response = new Response("Error", "There is something wrong in your request."." No airline data to seve: " . $payloadJson);
        }
        
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
     // </editor-fold>
}


// <editor-fold defaultstate="collapsed" desc="GV Data Table Classes">

class GvdtApByCountry extends GVDataTableBase {

    public function __construct() {
        parent::__construct();
        $mn = "GvdtApByCountry.construct()";
        AmsLogger::logBegin($mn);
        
        $this->cols = array();
        
        $this->cols[] = new GVColumn('country_name', "Country", GVColumnType::STRING);
        $this->cols[] = new GVColumn('country_iso2', "ISO", GVColumnType::STRING);
        $this->cols[] = new GVColumn('airports', "Airports", GVColumnType::NUMBER);
        $this->cols[] = new GVColumn('ap_active', "Active Airports", GVColumnType::NUMBER);
        $this->cols[] = new GVColumn('country_id', "Cuntry PK", GVColumnType::NUMBER);
       
        //parent::setCols($this->cols);
        $sql = " SELECT cr.country_name, cr.country_iso2, 
                    count(a.airport_id) as airports, sum(if(a.active>0,1,0) ) as ap_active,
                    cr.country_id
                FROM iordanov_ams_wad.air_airport a
                join iordanov_ams_wad.cfg_country cr on cr.country_id = a.country_id
                where 1=? 
                group by cr.country_id, cr.country_name, cr.country_iso2
                order by cr.country_name, cr.country_iso2, ap_active desc, airports desc ";
       
        $this->setSQL($sql);
        //parent::setSQL($sql);
        $this->getSql();
        AmsLogger::log($mn, " SQL = " . $this->getSql());
        $this->setBoundParamsArr(array('i', 1));
        $this->rows = array();
        AmsLogger::logEnd($mn);
    }

}

// </editor-fold>