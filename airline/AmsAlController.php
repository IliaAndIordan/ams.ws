<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsAlController.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 21.03.2016
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsAlController.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */

date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}

define('APP_HOME', dirname(dirname((__FILE__))));
define('SLASH', $slash);

ini_set("include_path", ini_get("include_path") . $delim . '/home/iordanov/php');

ini_set('include_path', ini_get('include_path') . $delim . '../' .
        $delim . '/home/iordanov/common/lib' . $delim . '/home/iordanov/common/lib/iiordan' .
        $delim . '/home/iordanov/common/lib/ams' .
        $delim . '/home/iordanov/common/lib/ams/al' .
        $delim . '/home/iordanov/common/lib/ams/models' .
        $delim . '/home/iordanov/common/lib/log4php' .
        $delim . '/home/iordanov/common//lib/log4php/configurators');



$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;

ini_set("display_errors", "1");

ob_start();

$mn = "AmsAlController.php";
//--- Include CORS
require_once("rest_cors_header.php");

//require_once("AmsWadConnection.php");
require_once("AmsLogger.php");
require_once("Functions.php");
//---Handlers
require_once("AmsAlRestHandler.class.php");
AmsLogger::logBegin($mn);

$view = "";
$id = null;

if (isset($_REQUEST["view"]))
    $view = $_REQUEST["view"];
if (isset($_REQUEST["id"]))
    $id = $_REQUEST["id"];

AmsLogger::log($mn, "view=" . $view . ", id=" . $id);

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    $restHendler = new AmsAlRestHandler();
    $restHendler->Option();
    AmsLogger::logEnd($mn);
} else {

    // get the HTTP method, path and body of the request
    $method = $_SERVER['REQUEST_METHOD'];
    AmsLogger::log($mn, "method=" . $method);
    // read JSon input
    $payloadJson;
    $payloadAuth;
    
    $authRes = JwtAuth::Autenticate();
    if (isset($authRes)) {
        if ($authRes->isValud) {
            $payloadAuth = $authRes->payload;
        } else {
            $response = new Response("error", $authRes->message);
            $response->statusCode = 401;
            $rh = new AmsAlRestHandler();
            $rh->EncodeResponce($response);
            return;
        }
    }
    
    try{
        $payload = file_get_contents('php://input');
        if(isset($payload)){
            $payloadJson = json_decode($payload);
            AmsLogger::log($mn, "payloadJson=" . $payload);
        }
    } catch (Exception $ex) {
        AmsLogger::logError($mn,  $ex);
    }
    
    /*
      controls the RESTful services URL mapping
     */
    switch ($view) {
         case "ping":
            // to handle REST Url /pcpd/
            $restHendler = new AmsAlRestHandler();
            $restHendler->Ping($id);
            AmsLogger::log($mn, "ping executed");
            break;
        case "al_list_for_check":
            // to handle REST Url /pcpd/
            $restHendler = new AmsAlRestHandler();
            $response = array();
            try {
                $restHendler->AlListForCheck();
            } catch (Exception $ex) {
                $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
            AmsLogger::log($mn, "response= " . $response);
            break;
        case "al_create":
            // to handle REST Url /pcpd/
            $restHendler = new AmsAlRestHandler();
            $response = array();
            try {
                //{"user_id":2,"al_name":"Ams System","al_code":"AMS","hq_airport_id":26413}
                $restHendler->AlCreate($payloadJson->user_id, $payloadJson->al_name, $payloadJson->al_code, $payloadJson->hq_airport_id);
            } catch (Exception $ex) {
                $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
            AmsLogger::log($mn, "response= " . $response);
            break;
        case "airline":
            // to handle REST Url /pcpd/
            $restHendler = new AmsAlRestHandler();
            $response = array();
            try {
                
                $restHendler->GetAirline($payloadJson->airline_id);
            } catch (Exception $ex) {
                $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
            AmsLogger::log($mn, "response= " . $response);
            break;
        case "al_save":
            // to handle REST Url /pcpd/
            $restHendler = new AmsAlRestHandler();
            $response = array();
            try {
                
                $restHendler->SaveAirline($payloadJson->airline);
            } catch (Exception $ex) {
                $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
            AmsLogger::log($mn, "response= " . $response);
            break;
        
        default:
            $response = new Response("error", "There is something wrong in your request."." No heandler for view: " . $view);
            AmsLogger::log($mn, "No heandler for view: " . $view);
            break;
    }
}
AmsLogger::logEnd($mn);
