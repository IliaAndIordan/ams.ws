<?php

date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}   

define('APP_HOME', dirname((__FILE__)));
define('SLASH', $slash);

//echo '\nAplication_home:'.APP_HOME.' \n';
ini_set("include_path",  ini_get("include_path") .$delim . '/home/iordanov/php');

ini_set('include_path',  ini_get('include_path') . 
        $delim . '/home/iordanov/common/lib' . $delim . '/home/iordanov/common/lib/iiordan'.
        $delim . '/home/iordanov/common/lib/log4php' . 
        $delim . '/home/iordanov/common//lib/log4php/configurators');


//display_errors = On
ini_set("display_errors", "1");
ob_start();

header('Cache-control: private');
header("Content-Type: text/html; charset=utf-8");
header('Access-Control-Allow-Origin: https://common.ams.iordanov.info/*');
session_start();

//echo '\n'.ini_get('include_path').' \n';    
?>

<!DOCTYPE HTML>
<html>
    <head>
        <title>☘ Web Services | Airline Manager Simulator</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Airline Manager Simulator">
        <meta name="author" content="I Iordanov">
        <link rel="shortcut icon" href="https://common.iordanov.info/images/ams/favicon_01.ico">
        <link rel="stylesheet" href="https://common.iordanov.info/css/admin/main.css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    <body>

        <!-- Header -->
        <div id="header">

            <div class="top">

                <!-- Logo -->
                <div id="logo">
                    <span class="image avatar48"><img src="https://common.iordanov.info/images/iziordanov_sd_128_bw.png" alt="izi" /></span>
                    <h1 id="title">I.Z.Iordanov</h1>
                    <p>Web Servicess AMS</p>
                </div>

                <div class="service_group">
                    <span class="image avatar24"><img src="https://common.iordanov.info/images/ams/common/airport.png" alt="airport" /></span>
                    <h1>Airport Service</h1>
                    <p></p>
                    <div class="service_group_bottom">
                        <ul class="service_group_icons">
                            <li><a href="https://ams.ws.ams.iordanov.info/airport/ping" class="icon material-icons" target="_blank" title="PING">done</a></li>
                            <li><a href="https://ams.ws.ams.iordanov.info/airport/countrylist" class="icon material-icons" target="_blank" title="Country List">location_city</a></li>
                            <li><a href="https://ams.ws.ams.iordanov.info/airport/airports_db_all" class="icon material-icons" target="_blank" title="All Airports from DB">group</a></li>

                        </ul>
                    </div>
                </div>
                <div class="service_group">
                    <span class="image avatar24"><img src="https://common.iordanov.info/images/ams/common/airline.png" alt="airport" /></span>
                    <h1>Airline Service</h1>
                    <p></p>
                    <div class="service_group_bottom">
                        <ul class="service_group_icons">
                            <li><a href="https://ams.ws.ams.iordanov.info/airline/ping" class="icon material-icons" target="_blank" title="PING">done</a></li>
                            <li><a href="https://ams.ws.ams.iordanov.info/airline/al_list_for_check" class="icon material-icons" target="_blank" title="Airline Name ICAO for check">check_box</a></li>
                        </ul>
                    </div>
                </div>
                <!-- Nav -->
                <nav id="nav">
                    <!--

                            Prologue's nav expects links in one of two formats:

                            1. Hash link (scrolls to a different section within the page)

                               <li><a href="#foobar" id="foobar-link" class="icon fa-whatever-icon-you-want skel-layers-ignoreHref"><span class="label">Foobar</span></a></li>

                            2. Standard link (sends the user to another page/site)

                               <li><a href="http://foobar.tld" id="foobar-link" class="icon fa-whatever-icon-you-want"><span class="label">Foobar</span></a></li>

                    -->
                    <ul>
                        <li><a href="#top" id="top-link" class="skel-layers-ignoreHref"><span class="icon fa-home">Intro</span></a></li>
                        <li><a href="#ams_wad" id="portfolio-link" class="skel-layers-ignoreHref"><span class="icon fa-th">Word Airports</span></a></li>
                        <li><a href="#about" id="about-link" class="skel-layers-ignoreHref"><span class="icon fa-user">About Me</span></a></li>
                        <li><a href="#contact" id="contact-link" class="skel-layers-ignoreHref"><span class="icon fa-envelope">Contact</span></a></li>
                    </ul>
                </nav>

            </div>

            <div class="bottom">

                <!-- Social Icons https://ams.ws.ams.iordanov.info/ping -->
                <ul class="icons">
                    <li><a href="https://ams.ws.ams.iordanov.info/ping" class="icon material-icons" target="_blank" title="PING">done</a></li>
                    <li><a href="https://ams.ws.ams.iordanov.info/register" class="icon material-icons" target="_blank" title="REGISTER">person_add</span></a></li>
                    <li><a href="https://ams.ws.ams.iordanov.info/login" class="icon material-icons" target="_blank" title="Login">perm_identity</span></a></li>
                    <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
                    <li><a href="#" class="icon fa-envelope"><span class="label">Email</span></a></li>
                    <li><a href="info_php.php" class="icon fa-envelope"><span class="label">PhP Info</span></a></li>
                </ul>

            </div>

        </div>

        <!-- Main -->
        <div id="main">

            <!-- Intro -->
            <section id="top" class="one dark cover">
                <div class="container">

                    <header>
                        <!--
                        <h2 class="alt"><strong>Web Services</strong>, for  
                            <a href="http://ams.iordanov.info">Airline Manager Simulator V 1</a> 
                        </h2>
                        -->
                    </header>



                </div>
            </section>

            <!-- WAD -->
            <section id="ams_wad" class="two">
                <div class="container">

                    <header>
                        <a href="http://ams.iordanov.info">Airline Manager Simulator V 1</a> 
                        <h2>Word Airport Database</h2>
                    </header>

                    <p>In order to start AMS project the need for base data of 
                        Country, Cities, Airports and runways are needed. This 
                        data collection we call World Airport Database. Available Services are:
                    </p>
                    <!--
                    <div class="row">
                        <div class="4u 12u$(mobile)">
                            <article class="item">
                                
                                <header>
                                    <h2>Country</h2>
                                    <h3><a href="http://wad.ws.ams.iordanov.info/country">List of All countries</a></h3>
                                    <h3><a href="http://wad.ws.ams.iordanov.info/country/iso2/BG">List of countries by ISO 2</a></h3>
                                    <h3><a href="http://wad.ws.ams.iordanov.info/country/region/Asia">List of countries by Continent/Region</a></h3>
                                    <h3><a href="http://wad.ws.ams.iordanov.info/country/subregion/Western%20Asia">List of countries by Sub Region</a></h3>
                                    <h3><a href="http://wad.ws.ams.iordanov.info/country/2">Country by ID</a></h3>
                                </header>
                            </article>
                            <article class="item">
                                <a href="#" class="image fit"><img src="images/pic03.jpg" alt="" /></a>
                                <header>
                                    <h3>Rhoncus Semper</h3>
                                </header>
                            </article>
                        </div>
                        <div class="4u 12u$(mobile)">
                            <article class="item">
                                <a href="#" class="image fit"><img src="images/pic04.jpg" alt="" /></a>
                                <header>
                                    <h3>Magna Nullam</h3>
                                </header>
                            </article>
                            <article class="item">
                                <a href="#" class="image fit"><img src="images/pic05.jpg" alt="" /></a>
                                <header>
                                    <h3>Natoque Vitae</h3>
                                </header>
                            </article>
                        </div>
                        <div class="4u$ 12u$(mobile)">
                            <article class="item">
                                <a href="#" class="image fit"><img src="images/pic06.jpg" alt="" /></a>
                                <header>
                                    <h3>Dolor Penatibus</h3>
                                </header>
                            </article>
                            <article class="item">
                                <a href="#" class="image fit"><img src="images/pic07.jpg" alt="" /></a>
                                <header>
                                    <h3>Orci Convallis</h3>
                                </header>
                            </article>
                        </div>
                    </div>

                </div>
                    -->
            </section>
             </div>
            <!-- Footer -->
        <div id="footer">

            <!-- Copyright -->
            <ul class="copyright">
                <li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
            </ul>

        </div>

        <!-- Scripts -->
        <script src="https://common.iordanov.info/templates/prologue/assets/js/jquery.min.js"></script>
        <script src="https://common.iordanov.info/templates/prologue/assets/js/jquery.scrolly.min.js"></script>
        <script src="https://common.iordanov.info/templates/prologue/assets/js/jquery.scrollzer.min.js"></script>
        <script src="https://common.iordanov.info/templates/prologue/assets/js/skel.min.js"></script>
        <script src="https://common.iordanov.info/templates/prologue/assets/js/util.js"></script>
        <!--[if lte IE 8]><script src="https://common.iordanov.info/templates/prologue/assets/js/ie/respond.min.js"></script><![endif]-->
        <script src="https://common.ams.iordanov.info/templates/prologue/assets/js/main.js"></script>

    </body>
</html>