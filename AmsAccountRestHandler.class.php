<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsAccountRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 21.03.2016
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsAccountRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once 'Response.class.php';
require_once("AmsConnection.php");
require_once("AmsLogger.php");
require_once("JwtAuth.php");
require_once("AmsUser.class.php");
require_once("UserModel.class.php");
require_once("AmsUserSettings.class.php");

require_once("AmsAirline.class.php");

/**
 * Description of AmsAccountRestHandler
 *
 * @author IZIordanov
 */
class AmsAccountRestHandler extends SimpleRest{
    
    // <editor-fold defaultstate="collapsed" desc="No JWT Tocken Methods">
    
    public function Option() {
        $mn = "AmsAccountRestHandler::Option()";
        $response = new Response("success", "Service working.");
        
        $rh = new AmsAccountRestHandler();
        $rh->EncodeResponce($response);
    }
    
    public function Ping() {
        $mn = "AmsAccountRestHandler::Ping()";
        AmsLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsConnection::dbConnect();
            if (isset($conn)) {
                AmsLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            }
            else
            {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }
            
        } catch (Exception $ex) {
            AmsLogger::logError($mn,  $ex);
            $response = new Response($ex);
        }
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function Login($email, $password) {
        $mn = "AmsAccountRestHandler::Login('.$email.', '.$password.')";
        AmsLogger::logBegin($mn);
        $response = null;
        try {
            $user = User::login($email, $password);
            if (isset($user)) {
                AmsLogger::log($mn, " user = " . $user->toJson());
                if(JwtAuth::signTocken($user->getId(), $user->getName())){
                    $response = new Response("success", "JwtAuth Set.");
                    $currUser = new UserModel($user);
                    
                    
                    $response->addData("current_user",$currUser);
                    $airline = AmsAirline::loadByUserId($user->getId());
                    $response->addData("airline",$airline);
                    
                    $us = new AmsUserSettings();
                    $us->loadById($user->getId());
                    $response->addData("settings",$us);
                    
                }
                else{
                   $response = new Response("error", "JwtAuth NOT Set.");
                   $response->statusCode=401;
                }
            }
            else
            {
                $response = new Response("error", "Invalid Credentials.");
                $response->statusCode=401;
            }
            
        } catch (Exception $ex) {
            AmsLogger::logError($mn,  $ex);
            $response = new Response($ex);
        }
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
     public function Register($eMail, $password)
    {
        $mn = "AmsAccountRestHandler::register('.$eMail.', '.$password.)";
        AmsLogger::logBegin($mn);
        $response = null;
        
        try {
            $wrongEmail = false;
            if(!isset($eMail) || !isset($password)){
                $wrongEmail = true;
            }
             
            if(!$wrongEmail ){
                $wrongEmail = !checkEmail($eMail);
                AmsLogger::log($mn, "checkEmail = " . $wrongEmail?'true':'false');
            }
            
            if(!$wrongEmail){
                $wrongEmail=User::isExistEMail($eMail);
                if($wrongEmail){
                     $response = new Response("error", "E-Mail already registered.");
                     $response->statusCode = 200;
                }
            }
            
            
            if (!$wrongEmail) {
                $user = User::CreateRowData($eMail, $password);
                
                AmsLogger::log($mn, "new user = " . $user->toJSON());
                 if (JwtAuth::signTocken($user->getId(), $user->getName())) {
                    $response = new Response("success", "JwtAuth Set.");
                    $currUser = new UserModel($user);

                    $response->addData("current_user", $currUser);
                } else {
                    $response = new Response("error", "JwtAuth NOT Set.");
                    $response->statusCode = 401;
                }
            } else {
                $response = new Response("error", "Invalid Credentials.");
                $response->statusCode = 200;
            }

        } catch (Exception $ex) {
            AmsLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    
    public function refreshToken() {
        $mn = "AmsAccountRestHandler::refreshToken()";
        AmsLogger::logBegin($mn);
        $response = null;
        try {
            
            $autRes = JwtAuth::Autenticate();
           
            if(isset($autRes))
            {
                $success = JwtAuth::RefreshTocken($autRes);
                if($success){
                    $payload = $autRes->payload;
                    $user = new User();
                    $user->loadById(intval($payload->data->user_id));
                    if (isset($user)) {
                        AmsLogger::log($mn, " user = " . $user->toJson());
                        
                        AmsLogger::log($mn, " user = " . $user->toJson());
                        if(JwtAuth::signTocken($user->getId(), $user->getName())){
                            $response = new Response("success", "JwtAuth Set.");
                            $currUser = new UserModel($user);


                            $response->addData("current_user",$currUser);
                            $airline = AmsAirline::loadByUserId($user->getId());
                            $response->addData("airline",$airline);

                            $us = new AmsUserSettings();
                            $us->loadById($user->getId());
                            $response->addData("settings",$us);

                        }
                        else{
                           $response = new Response("error", "JwtAuth NOT Set.");
                           $response->statusCode=401;
                        }
                    } 
                    else{
                       $response = new Response("error", "JwtAuth User data invalid.");
                       $response->statusCode=401;
                    }
                } 
                else{
                    $response = new Response("error", "JwtAuth RefreshTocken invalid.");
                    $response->statusCode=401;
                 }
            } 
            else{
                $response = new Response("error", "JwtAuth Autenticate invalid.");
                $response->statusCode=401;
             }
            
            
            
        } catch (Exception $ex) {
            AmsLogger::logError($mn,  $ex);
            $response = new Response($ex);
        }
        //AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
     // </editor-fold>
}
