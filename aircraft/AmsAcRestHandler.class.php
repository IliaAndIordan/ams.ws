<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsAcRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 21.03.2016
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsAcRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("Functions.php");
require_once("SimpleRest.class.php");
require_once 'Response.class.php';
require_once("AmsConnection.php");
require_once("AmsLogger.php");
require_once("JwtAuth.php");
require_once("AmsUser.class.php");
require_once("AmsAirline.class.php");
require_once("AmsAcMfr.class.php");
require_once("AmsMac.class.php");
require_once("UserModel.class.php");
require_once 'GVDataTable.class.php';

/**
 * Description of AmsAcRestHandler
 *
 * @author IZIordanov
 */
class AmsAcRestHandler extends SimpleRest{
    
    // <editor-fold defaultstate="collapsed" desc="No JWT Tocken Methods">
    
    public function Option() {
        $mn = "AmsAcRestHandler::Option()";
        $response = new Response("success", "Service working.");
        
        $rh = new AmsAcRestHandler();
        $rh->EncodeResponce($response);
    }
    
    public static function AuthenticationRequired() {
        $mn = "AmsAcRestHandler::AuthenticationRequired()";
        $response = new Response("error", "Authentication Required.");
        $response->statusCode=401;
        $rh = new AmsAcRestHandler();
        $rh->EncodeResponce($response);
    }
    
    public function Ping() {
        $mn = "AmsAcRestHandler::Ping()";
        AmsLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsConnection::dbConnect();
            if (isset($conn)) {
                AmsLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            }
            else
            {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }
            
        } catch (Exception $ex) {
            AmsLogger::logError($mn,  $ex);
            $response = new Response($ex);
        }
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function AcMfrList() {
        $mn = "AmsAcRestHandler::AcMfrList()";
        AmsLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($mn);
            $sql = "SELECT m.mfr_id, m.mfr_name, m.mfr_headquartier, m.mfr_notes, 
                    m.mfr_wikilink, m.mfr_web, m.mfr_logo, m.airport_id, m.mfr_iata,
                    a.iata as ap_iata, a.icao as ap_icao, a.aname as ap_name, a.lat as ap_lat, a.lon as ap_lon,
                    a.country_id, c.country_iso2, c.country_name, c.region, c.sub_region,
                    a.state_id, cs.state_name, 
                    a.city_id, ct.city_name, ct.wiki_url as city_wiki_url
                    FROM iordanov_ams_ac.cfg_manufacturer m
                    join iordanov_ams_wad.air_airport a on a.airport_id = m.airport_id
                    join iordanov_ams_wad.cfg_country c on c.country_id = a.country_id
                    left join iordanov_ams_wad.cfg_country_state cs on cs.state_id = a.state_id
                    left join iordanov_ams_wad.cfg_city ct on ct.city_id = a.city_id
                    where 1=?
                    order by m.mfr_name
                    ";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("ac_mfr_list",$ret_json_data);
            
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    
     // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="No JWT Tocken Methods">
    
    public function MacList() {
        $mn = "AmsAcRestHandler::MacList()";
        AmsLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($mn);
            $sql = "SELECT ma.mac_id, ma.mfr_id, ma.model , ma.ac_type_id, ma.max_range_km, ma.fuel_tank_l,
                    ma.pilots, ma.crew_seats, ma.max_seating, ma.useful_load_kg, ma.price, ma.avg_fuel_consumption_lp100km,
                    ma.max_cruise_altitude_m, ma.cruise_speed_kmph, ma.take_off_m, ma.landing_m, ma.operation_time, ma.cost_per_fh,
                    ma.asmi_rate, ma.wiki_link, ma.powerplant, ma.production_start, ma.production_rate_h, ma.last_produced_on,
                    ma.notes, ma.popularity, ma.number_build, ma.mtwo_kg, ma.mtow_empty, ma.seats_per_row, ma.cabin_config_base_price,
                    t.ac_type_name
                    FROM iordanov_ams_ac.cfg_mac ma
                    join iordanov_ams_ac.cfg_aircraft_type t
                    where 1=?
                    order by ma.price, ma.max_seating, ma.max_range_km
                    ";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("mac_list",$ret_json_data);
            
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        AmsLogger::log($mn, " response = " . $response->toJSON());
        AmsLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
}


// <editor-fold defaultstate="collapsed" desc="GV Data Table Classes">

class GvdtApByCountry extends GVDataTableBase {

    public function __construct() {
        parent::__construct();
        $mn = "GvdtApByCountry.construct()";
        AmsLogger::logBegin($mn);
        
        $this->cols = array();
        
        $this->cols[] = new GVColumn('country_name', "Country", GVColumnType::STRING);
        $this->cols[] = new GVColumn('country_iso2', "ISO", GVColumnType::STRING);
        $this->cols[] = new GVColumn('airports', "Airports", GVColumnType::NUMBER);
        $this->cols[] = new GVColumn('ap_active', "Active Airports", GVColumnType::NUMBER);
        $this->cols[] = new GVColumn('country_id', "Cuntry PK", GVColumnType::NUMBER);
       
        //parent::setCols($this->cols);
        $sql = " SELECT cr.country_name, cr.country_iso2, 
                    count(a.airport_id) as airports, sum(if(a.active>0,1,0) ) as ap_active,
                    cr.country_id
                FROM iordanov_ams_wad.air_airport a
                join iordanov_ams_wad.cfg_country cr on cr.country_id = a.country_id
                where 1=? 
                group by cr.country_id, cr.country_name, cr.country_iso2
                order by cr.country_name, cr.country_iso2, ap_active desc, airports desc ";
       
        $this->setSQL($sql);
        //parent::setSQL($sql);
        $this->getSql();
        AmsLogger::log($mn, " SQL = " . $this->getSql());
        $this->setBoundParamsArr(array('i', 1));
        $this->rows = array();
        AmsLogger::logEnd($mn);
    }

}

// </editor-fold>