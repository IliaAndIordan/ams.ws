<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
global $AmsLogger;

require_once("LoggerBase.php");
Logger::configure(dirname(__FILE__).'/appender_pdo.properties');
/**
 * Description of Log
 *
 * @author izior
 */
class AmsLogger extends LoggerBase {
    
    // <editor-fold defaultstate="collapsed" desc="__construct">
    public function __construct() {
        parent::__construct();
        $this->MN = "AmsLogger: ";
        try {
            
            
            $this->logger = Logger::getRootLogger();
           
            $this->logger->debug("AmsLogger init");
           
        } catch (Exception $ex) {
            echo "AmsLogger Error: " . $ex."<br/>";
        }
        //logEndST($MN, $ST);
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function currLogger(){
         global $AmsLogger;
        if(!isset($AmsLogger))
        {
            $AmsLogger = new AmsLogger();
        }
        
        return $AmsLogger;
    }
    
    public static function logBegin($mn)
    {
        AmsLogger::currLogger()->begin($mn);
    }
    
     public static function logEnd($mn)
    {
        AmsLogger::currLogger()->end($mn);
    }
    
     public static function log($mn, $msg)
    {
        AmsLogger::currLogger()->debug($mn,$msg);
    }
    
    public static function logError($mn, $ex)
    {
        AmsLogger::currLogger()->error($mn, $ex);
    }
    // </editor-fold>
}
